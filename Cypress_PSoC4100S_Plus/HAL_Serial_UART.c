//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019

\file       SerialComm_HAL.c
\brief      Module for low level serial communication initialization.

***********************************************************************************/
#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS
    
#include <project.h>
#include "HAL_Serial_UART.h"

/****************************************** Defines ******************************************************/
/****************************************** Variables ****************************************************/
static pFunctionParamU8 pFnOnRxData = NULL;     /* Function pointer for handling received data */
static pFunction pFnOnTxComplete = NULL;        /* Function pointer for handling completed transmit */

/****************************************** Function prototypes ******************************************/

/****************************************** local functiones *********************************************/
//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019
\brief      ISR function called by hardware whenever a byte is received or
            has been completely transmitted.
\return     none
\param      none
***********************************************************************************/
static void UART_Isr(void)
{
    /* Check if interrupt source is from RX */
    if(UART_CHECK_CAUSE_INTR(UART_INTR_CAUSE_RX))
    {
        /* Get the RX-interrupt cause (like full fifo..)*/
        u32 ulIsrCause = UART_GetRxInterruptSource();

        /* Check if Receiver FIFO is not empty */
        if(ulIsrCause & UART_INTR_RX_NOT_EMPTY)
        {
            /* Read RX-FIFO data */
            u32 ulRxFifoData = UART_SpiUartReadRxData();
            
            /* Check first if RX-Callback is set */
            if(pFnOnRxData)
            {
                /* Use callback for RX data receive */
                pFnOnRxData(ulRxFifoData);
            }
        }
        
        /* Clear RX isr flag */
        UART_ClearRxInterruptSource(ulIsrCause);
    }

    /* Check if interrupt source is from TX */
    if(UART_CHECK_CAUSE_INTR(UART_INTR_CAUSE_TX))
    {
        /* Get the TX-interrupt cause (like full fifo..)*/
        u32 ulIsrCause = UART_GetTxInterruptSource();
        
        /* Check first if TX-Callback is set */
        if(pFnOnTxComplete)
        {
            pFnOnTxComplete();
        }
        
        /* Clear TX isr flag */
        UART_ClearTxInterruptSource(ulIsrCause);
    }
}


/****************************************** External visible functiones **********************************/

//********************************************************************************
/*!
\author     Kraemer E
\date       26.06.2021
\brief      Reads the RX-Buffer und calls the saved callback function.
\return     void 
\param      none
***********************************************************************************/
void HAL_Serial_UART_PollRxData(void)
{
    /* Get the amount of available data in the RX buffer */
    while(UART_SpiUartGetRxBufferSize())
    {
        /* Read single RX-FIFO data */
        u32 ulRxFifoData = UART_SpiUartReadRxData();
        
        /* Check first if RX-Callback is set */
        if(pFnOnRxData)
        {
            /* Use callback for RX data receive */
            pFnOnRxData(ulRxFifoData);
        }
        else
        {
            //No callback set
            break;
        }
    }
}


//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019
\brief      Initializes the serial HAL module by setting the callbacks,
            and calls hardware initialization functions.
\return     void 
\param      pFnOnRx  - callback for received bytes
\param      pFnOnTx  - callback for transmission complete
***********************************************************************************/
void HAL_Serial_UART_Init(pFunctionParamU8 pCallbackOnRx, pFunction pCallbackOnTx)
{
    pFnOnRxData = pCallbackOnRx;
    pFnOnTxComplete = pCallbackOnTx;

    UART_Start();  
    UART_ISR_StartEx(UART_Isr);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019
\brief      HAL function for enabling/disabling the receive interrupt.
\return     none 
\param      bEnable - True for enable and false for disable
***********************************************************************************/
void HAL_Serial_UART_EnableRxIrq(bool bEnable)
{
    /* Returns RX interrupt mask register. This register specifies which bits from 
     *  RX interrupt request register will trigger an interrupt event.*/
    u32 ulIrqMask = UART_GetRxInterruptMode();

    /* Check if RX IRQ shall be triggered by a "NOT_EMPTY" interrupt */
    if(bEnable)
    {
        ulIrqMask |= UART_INTR_RX_NOT_EMPTY;
    }
    else
    {
        ulIrqMask &= !UART_INTR_RX_NOT_EMPTY;
    }

    /* Set interrupt mask */
    UART_SetRxInterruptMode(ulIrqMask);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019
\brief      HAL function for enabling/disabling the transmit-complete interrupt
\return     none 
\param      bEnable - True for enable and false for disable
***********************************************************************************/
void HAL_Serial_UART_EnableTxIrq(bool bEnable)
{
    /* Returns TX interrupt mask register. This register specifies which bits from 
     * TX interrupt request register will trigger an interrupt event. */
    u32 ulIrqMask = UART_GetTxInterruptMode ();

    /* Check if TX IRQ shall be triggered by a "UART_DONE" interrupt */
    if(bEnable)
    {
        ulIrqMask |= UART_INTR_TX_UART_DONE;
    }
    else
    {
        ulIrqMask &= !UART_INTR_TX_UART_DONE;
    }

    UART_SetTxInterruptMode(ulIrqMask);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019
\brief      Writes just one byte to the TX register
\return     void 
\param      ucByte  - data byte
***********************************************************************************/
void HAL_Serial_UART_WriteByte(u8 ucByte)
{
    UART_SpiUartWriteTxData(ucByte);
}

//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019
\brief      Writes given buffer to the TX register
\return     void 
\param      pucBuffer  - Pointer to the buffer
\param      ucBufferCnt - Amount of elements to be transmitted
***********************************************************************************/
void HAL_Serial_UART_WriteBuffer(const u8* pucBuffer, u8 ucBufferCnt)
{
    UART_SpiUartPutArray(pucBuffer, ucBufferCnt);
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       20.02.2019
\brief      Enables the UART wakeup source for the deep sleep.
***********************************************************************************/
void HAL_Serial_UART_EnableUartWakeup(void)
{
    /* Set skip mode */
    #if (UART_2_UART_WAKE_ENABLE_CONST && UART_2_UART_RX_WAKEUP_IRQ)
        UART_2_skipStart = SKIP_FIRST_START_BIT_IN_SLEEP;
    #endif
    
    /* Wait for UART buffer to be done */    
//    while(0 != UART_2_SpiUartGetTxBufferSize + UART_2_GET_TX_FIFO_SR_VALID);
    
    /* Put UART module into wakeup source for sleep mode */
    UART_Sleep();
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       20.02.2019
\brief      Disables the UART wakeup source for the deep sleep.
***********************************************************************************/
void HAL_Serial_UART_DisableUartWakeup(void)
{
    /* Restore UART module from sleep mode */
    UART_Wakeup();
}

//********************************************************************************
/*!
\author     Kraemer E.
\date       20.02.2019
\brief      Prints the character string directly onto the UART
***********************************************************************************/
void HAL_Serial_UART_Printf(char* szString)
{
    /* Create pointer to get the size */
    char* ptr = szString;
    u8 ucStringSize = 0;
    
    while(*ptr != 0)
    {
        ++ucStringSize;
        ++ptr;
    }
    
    UART_SpiUartPutArray((u8*)szString, ucStringSize);
}

#endif //PSOC_4100S_PLUS