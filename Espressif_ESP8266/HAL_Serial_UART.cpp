//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019

\file       SerialComm_HAL.c
\brief      Module for low level serial communication initialization.

***********************************************************************************/
#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266

#include "HAL_Serial_UART.h"
#include "Arduino.h"

/****************************************** Defines ******************************************************/
#define SERIAL_BAUDRATE         38400

/****************************************** Variables ****************************************************/
static pFunctionParamU8 pFnOnRxData = NULL;     /* Function pointer for handling received data */
static pFunction pFnOnTxComplete = NULL;        /* Function pointer for handling completed transmit */

/****************************************** Function prototypes ******************************************/

/****************************************** local functiones *********************************************/

/****************************************** External visible functiones **********************************/
//********************************************************************************
/*!
\author     Kraemer E
\date       26.06.2021
\brief      Reads the RX-Buffer und calls the saved callback function.
            ISR function called by hardware whenever a byte is received.
            This is an Arduino-API! Which has to be called cyclically
\return     none
\param      none
***********************************************************************************/
void HAL_Serial_UART_PollRxData(void)
{
    /* Check if Receiver FIFO is not empty */
    while(Serial.available())
    {
        /* Read RX-FIFO data */
        u32 ulRxFifoData = Serial.read();
        
        /* Check first if RX-Callback is set */
        if(pFnOnRxData)
        {
            /* Use callback for RX data receive */
            pFnOnRxData(ulRxFifoData);
        }
        else
        {
            //No callback set
            break;
        }
    }
}

//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019
\brief      Initializes the serial HAL module by setting the callbacks,
            and calls hardware initialization functions.
\return     void 
\param      pFnOnRx  - callback for received bytes
\param      pFnOnTx  - callback for transmission complete
***********************************************************************************/
void HAL_Serial_UART_Init(pFunctionParamU8 pCallbackOnRx, pFunction pCallbackOnTx)
{
    pFnOnRxData = pCallbackOnRx;
    pFnOnTxComplete = pCallbackOnTx;

    //Start with the SERIAL-COMM
    Serial.begin(SERIAL_BAUDRATE);  

    /* Start own event for UART */
    //See Arduinos "serialEvent()" which can be used therefore
    //otherwise poll "serialEvent()" with another function call in the main loop
    //Serial_HAL_SerialEvent is called in the main loop! Because ISR function doesn't work!!!
}

//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019
\brief      Writes just one byte to the TX register
\return     void 
\param      ucByte  - data byte
***********************************************************************************/
void HAL_Serial_UART_WriteByte(u8 ucByte)
{
    /* Wait for write buffer to has free slots */
    while(Serial.availableForWrite())
    { 
       /* Transmitt byte */
       Serial.write(ucByte); 
       break;
    }
}


//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019
\brief      Prints a string literal on the UART
\return     void 
\param      szString - String literal
***********************************************************************************/
void HAL_Serial_UART_Printf(char* szString)
{
    Serial.println(szString);
}

void HAL_Serial_UART_EnableRxIrq (bool bEnable)
{
    //No interrupt used
}

void HAL_Serial_UART_EnableTxIrq (bool bEnable)
{
    //No interrupt used
}

void HAL_Serial_UART_EnableUartWakeup(void)
{
    //No UART wakeup possible
}

void HAL_Serial_UART_DisableUartWakeup(void)
{
    //No UART wakeup possible
}

#endif //ESPRESSIF_ESP8266