//********************************************************************************
/*!
\author     Kraemer E
\date       21.01.2019

\file       HAL_Serial_UART
\brief      Module for low level serial communication initialization.

***********************************************************************************/


#ifndef _HAL_SERIAL_UART_H_
#define _HAL_SERIAL_UART_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include "BaseTypes.h"

void HAL_Serial_UART_PollRxData(void);
void HAL_Serial_UART_Init(pFunctionParamU8 pCallbackOnRx, pFunction pCallbackOnTx);
void HAL_Serial_UART_WriteByte(u8 ucByte);
void HAL_Serial_UART_EnableRxIrq (bool bEnable);
void HAL_Serial_UART_EnableTxIrq (bool bEnable);
void HAL_Serial_UART_EnableUartWakeup(void);
void HAL_Serial_UART_DisableUartWakeup(void);

void HAL_Serial_UART_Printf(char* szString);

#ifdef __cplusplus
}
#endif

#endif // _HAL_SERIAL_UART_H_
